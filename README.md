# Prerequisites
The dependencies are locked using yarn.
To avoid possible packages incompatibility or other issues, please use yarn as well.

# Install
Just run `yarn install` in the root directory.
There is a `post-install` hook that installs API dependencies as well.
For simplicity, there is one repo (API has own package.json).

# Running
In order to start this demo you must to run two commands:

- `yarn start`
- `yarn run server:start` or `yarn start` in the `/api` folder
## Preview
See `./demo-screencast.mp4`

# Web APP
Almost every React app i wrote with redux but not this time.
I could for example:

 - have a single action to fetch the data (action creator with redux-thunk) triggered by the spin button
 - store the slot machine state in the redux store
 - have a selector to map the data to the SlotMachine props

I decided to store the data in the component.
I loved that the entire application state sits in one place (single store provider) and can be simply used in any new component.
It turned out sometimes this is really painful.
For example if i want to have a multiple slot machines running independently on the same page i need to map component instance somehow with the machine state.
Anyway, thanks to new official Context API you might not need redux at all.

## Naming convention
I was struggling with this for a while.
I used to write CamelCase for every component so the filename reflects the class name.
In fact it turns out that i have something like `app/some-module/components/ComponentName`, `shared/api/ApiTrigger`.
I don't want to either have a multiple naming conventions or name every folder with camel case.
I picked a `dash-case` convention for file naming so at least there is one convention for file names.
PS. This is something that works for angular already.

## Architectural decisions

### API connection
There is a API Context that provides a host, baseUrl and the fetch adapter so no need to hardcode it somewhere else.
Currently i wrote one component using a API Consumer that fetches the data when the button is clicked (see [ApiTrigger](./src/shared/api/api-trigger.tsx) for details).

### The game state
I always use selectors so the data are mapped/prepared to the exact form component needs to exist.
In this case to display a slot symbol i only need a text and flag whether it is active or not like:
```
{
  reels: Array<{ slots: Array<{ symbol: string, active: boolean }}>>
}
```
BTW. The selector does not care about the winning symbols.
It finds appropriate checking the coords against the reel symbols.

### Slot machine
The slot machine consists of a few pure components (panel, reel, slot).

### Styling
I tried to not focus on the styles.
Honestly, haven't used css modules, jss etc. yet on production.

# API
API server sits in the `/api` directory.

## Architecture
The API is built with TypeScript.
The architecture is as simple as possible.
For example, the controller handler and the routes are lean on purpose.
I don't want to support asynchronous controllers or defining the nested routes until necessary.
The main package.json has definitions for the API commands as well so you can just run "yarn run server:test".
It is hard to find the best way without knowledge about both the scope and the business domain.
Anyway, it's always a problem to pick the proper and should be discussed within a team.
I didn't find the perfect architecture yet.

For the larger project, I would rather split code to modules instead of having one bag for all of the controllers/services.
It could look like:

- `/modules/spin/controllers|services`
- `/shared/services/auth|db|etc`

## Development
`ts-node` should not be used on production, so you need to transpile sources first using `tsc`.

# Tests
I've created tests in the separate tests because I used to.
It's easy then to eg adjust tslint only for the tests like:
```
  "no-object-literal-type-assertion": false,
  "no-implicit-dependencies": [true, "dev"]
```
BTW. I don't have any objections keeping the unit tests together with the tested module.
## Jest
I'm using jest v23 because the latest is not compatible with the current ts-node. It works but I don't like to see any warnings.
jest 24 is the latest version (02.02.2019)
## JSON schemas
I did not want to waste time polishing it, I've just generated a schema with https://www.jsonschema.net/.
There are some problems visible with the naked eye, including missing `additionalProperties=false` flag and redundant schema for the symbols but it can be simply written once and reused via the `$ref` keyword.

# Code style
## TSlint
I don't have any favorite tslint rules and won't focus to setup it thoroughly.
It's something that should be discussed & agreed within the team.

# STATUS
I spend around 15-20hours and cannot make more for now.

## TODOS
There is a lot of thigs i haven't done yet.
I did not have enough time for:

- More tests for the react components & selector.
- Use css-modules.
- Configure environment variables like API port/host.
- Possible HOC for the SpinButton.
- Decouple api from the web client. Currently i'm extending tslint/ts configs.
- Prepare production builds (api: tsc, web: chunks, optimizers etc)
- Check for errors in the README file.
- Pre commit/push hooks running linting and possible tests.
- Find some better solution to increment the winnings in the react component.
- Fix some line endings issues (CRLF/LF). just had some problems with autocrlf. I've just found some but don't have time to look into it now.
- ???
