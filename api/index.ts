import express from 'express';
import { createApp } from './src/createApp';

const app = express();

createApp(app)
  .listen(process.env.API_PORT || 8081);
