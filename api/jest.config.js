'use strict';

module.exports = {
  clearMocks: true,
  verbose: true,
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testRegex: 'tests/.*\\.test\\.ts$',
  moduleFileExtensions: ['js', 'ts'],
  coverageReporters: ['text'],
  collectCoverageFrom: [
    'src/**/*.ts',
    '!**/node_modules/**',
  ],
};
