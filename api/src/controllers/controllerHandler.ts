import { NextFunction, Request, Response, Handler } from 'express';

export const controllerHandler = (fn: Handler) => {
  if (typeof fn !== 'function') {
    throw new Error('[controllerHandler] Provided function must be a function');
  }
  return (req: Request, res: Response, next: NextFunction) => {
    try {
      fn(req, res, next);
    } catch (err) {
      next(err);
    }
  };
};
