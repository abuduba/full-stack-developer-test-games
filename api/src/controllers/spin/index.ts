import { spinPost } from './spinPost';
import { Controllers } from '../../model';

export const spin: Controllers<'post'> = { post: spinPost };
