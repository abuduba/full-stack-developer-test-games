import { Handler, Request, Response } from 'express';

import { generateSpin } from '../../services/spin/generateSpin';

export const spinPost: Handler = (_: Request, res: Response): void => {
  res.json(generateSpin());
};
