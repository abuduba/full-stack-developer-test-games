import { Express } from 'express';

import { configureRoutes } from './routes/configureRoutes';
import { routes } from './routes/routes';

export const createApp = (app: Express): Express => {
  configureRoutes(app, routes);
  return app;
};
