import { Handler, Express } from 'express';

type Method = keyof Pick<Express, 'post' | 'get' | 'delete'>;
export interface IRoute {
  method: Method;
  path: string;
  controller: Handler;
}

export type Controllers<T extends Method> = Record<T, Handler>;
export type Service = (...args: any[]) => any;
