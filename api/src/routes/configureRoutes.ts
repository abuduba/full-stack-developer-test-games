import { Router } from 'express';

import { IRoute } from '../model';
import { controllerHandler } from '../controllers/controllerHandler';

const configureRoute = (router: Router, route: IRoute) => {
  router[route.method](
    route.path,
    controllerHandler(route.controller),
  );
};

export const configureRoutes = (router: Router, routes: IRoute[]) =>
  routes.forEach((route) => { configureRoute(router, route); });
