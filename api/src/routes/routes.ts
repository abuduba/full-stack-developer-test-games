import { spin } from '../controllers/spin';
import { IRoute } from '../model';

export const routes: IRoute[] = [
  {
    controller: spin.post,
    method: 'post',
    path: '/api/spin',
  },
];
