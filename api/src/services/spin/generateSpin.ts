import { Service } from '../../model';

import { ISlotMachineStateResponse, IReel, ICoord, IWinning } from '../../../../src/model';
import { times, sample, random, range } from 'lodash';

const availableSymbols: string[] = range(
  'A'.charCodeAt(0),
  'Z'.charCodeAt(0) + 1,
).map((charCode: number) => {
  const char = String.fromCharCode(charCode);
  return char + char;
});

const reelsNumber: number = 5;
const slotsNumber: number = 3;
const maxWinnings: number = 5;
const winningFactorValue = 15;
const maxWinningFactor = 4;

const generateReel = (): IReel => ({
  symbols: times<string>(slotsNumber, () => sample(availableSymbols) as string),
});

const generateWinning = (reels: IReel[]): IWinning => {
  const symbols: string[] = [];
  const coords: ICoord[] = [];
  times(reelsNumber, (i: number) => {
    const slotIndex = random(slotsNumber - 1);
    const symbol = reels[i].symbols[slotIndex];
    symbols.push(symbol);
    coords.push({ reel: i, row: slotIndex });
  });

  return {
    winnings: winningFactorValue * (random(maxWinningFactor - 1) + 1),
    symbols,
    coords,
  };
};

const generateWinnings = (reels: IReel[]) => {
  const winningsNumber: number = random(maxWinnings);
  return times(winningsNumber, () => generateWinning(reels));
};

export const generateSpin: Service = (): ISlotMachineStateResponse => {
  const reels = times(reelsNumber, generateReel);
  return {
    gameState: {
      panel: {
        reels,
      },
      winnings: generateWinnings(reels),
    },
  };
};
