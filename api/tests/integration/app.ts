import express from 'express';
import { createApp } from '../../src/createApp';

export const app: express.Express = createApp(express());
