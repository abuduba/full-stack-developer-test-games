import request from 'supertest';
import { validate } from 'jsonschema';

import { app } from '../app';
import * as spinSchema from '../../schema/spin.schema.json';

describe('POST /spin should', () => {
  it('return OK response', async () => {
    await request(app)
      .post('/api/spin')
      .expect(200);
  });

  it('return a spin data that matches JSON schema', async () => {
    const { body } = await request(app)
      .post('/api/spin');

    validate(body, spinSchema, { throwError: true });
  });
});
