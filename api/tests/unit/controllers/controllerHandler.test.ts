import { Request, Response, NextFunction } from 'express';

import { controllerHandler } from '../../../src/controllers/controllerHandler';

describe('Controller Handler should', () => {
  it.each([
    undefined,
    5,
  ])('throw when non-function argument (%s) is provided', (arg) => {
    expect(() => (controllerHandler as any)(arg)).toThrow('[controllerHandler] Provided function must be a function');
  });

  it('return a function', () => {
    expect(controllerHandler(() => null)).toBeInstanceOf(Function);
  });

  describe('wrap given function and', () => {
    it('return undefined', () => {
      expect(controllerHandler(() => null)({} as Request, {} as Response, () => null)).toBe(undefined);
    });

    it('pass through the request, respone and the next function', () => {
      const req: Request = {} as Request;
      const res: Response = {} as Response;
      const next: NextFunction = () => null;
      const fn = jest.fn();

      controllerHandler(fn)(req, res, next);

      expect(fn).toHaveBeenCalledWith(req, res, next);
    });

    it('catch error and pass it to the next function', () => {
      expect.assertions(1);
      const error = new Error('Some error');
      const fn = () => { throw error; };
      const next = jest.fn();

      controllerHandler(fn)({} as Request, {} as Response, next);

      expect(next).toHaveBeenCalledWith(error);
    });
  });
});
