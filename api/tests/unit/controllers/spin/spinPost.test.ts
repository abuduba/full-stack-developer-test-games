jest.mock('../../../../src/services/spin/generateSpin');

import { Send, Response, Request } from 'express';

import { spinPost } from '../../../../src/controllers/spin/spinPost';
import * as spinService from '../../../../src/services/spin/generateSpin';

describe('[Controller] spin.spinPost should', () => {
  beforeEach(() => {
    (spinService.generateSpin as jest.Mock).mockClear();
  });

  it('return a mock response', () => {
    const json: Send = jest.fn() as Send;
    const res: Response = { json } as Response;
    const responseData = { someTest: 'data' };
    (spinService.generateSpin as jest.Mock).mockReturnValueOnce(responseData);

    spinPost({} as Request, res, () => null);

    expect(json).toHaveBeenCalledWith(responseData);
  });
});
