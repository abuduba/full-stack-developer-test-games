jest.mock('../../src/routes/configureRoutes.ts');
jest.mock('../../src/routes/routes.ts');

import { Express } from 'express';

import { configureRoutes } from '../../src/routes/configureRoutes';
import { routes } from '../../src/routes/routes';
import { createApp } from '../../src/createApp';

describe('createApp should', () => {
  it('configure routes', () => {
    const app: Express = {  } as Express;
    createApp(app);
    expect(configureRoutes).toHaveBeenCalledWith(app, routes);
  });

  it('return the configured app', () => {
    const app: Express = {} as Express;
    expect(createApp(app)).toBe(app);
  });
});
