jest.mock('../../../src/controllers/controllerHandler', () => ({ controllerHandler: jest.fn() }));

import { Router, IRouterMatcher } from 'express';
import { when } from 'jest-when';

import { configureRoutes } from '../../../src/routes/configureRoutes';
import { controllerHandler } from '../../../src/controllers/controllerHandler';

describe('configureRoutes should', () => {
  beforeEach(() => {
    (controllerHandler as jest.Mock).mockClear();
  });

  it('configure the routes on the express router instance', () => {
    const router: Router = { get: jest.fn() as IRouterMatcher<any> } as Router;
    const controller = () => null;
    const wrappedController = { whatever: 123 };
    when(controllerHandler)
      .calledWith(controller)
      .mockReturnValueOnce(wrappedController);

    configureRoutes(router, [{ method: 'get', path: '/abc', controller }]);

    expect(router.get).toHaveBeenCalledWith('/abc', wrappedController);
  });
});
