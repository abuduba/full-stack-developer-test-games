import { routes } from '../../../src/routes/routes';
import { spinPost } from '../../../src/controllers/spin/spinPost';

describe('routes should', () => {
  it('match snapshot', () => {
    expect(routes).toMatchObject([
      {
        path: '/api/spin',
        controller: spinPost,
        method: 'post',
      },
    ]);
  });
});
