import { validate } from 'jsonschema';

import { generateSpin } from '../../../../src/services/spin/generateSpin';
import * as spinSchema from '../../../schema/spin.schema.json';

describe('[Service] spin.generateSpin should',  () => {
  it('return a valid spin response response', () => {
    validate(generateSpin(), spinSchema, { throwError: true });
  });
});
