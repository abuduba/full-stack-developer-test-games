'use strict';

module.exports = {
  clearMocks: true,
  verbose: true,
  snapshotSerializers: ['enzyme-to-json/serializer'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testPathIgnorePatterns: [
    '/node_modules/',
    '/api/'
  ],
  testRegex: 'tests/.*\\.test\\.tsx?$',
  moduleFileExtensions: ['js', 'ts', 'tsx'],
  coverageReporters: ['text'],
  setupFiles: ['./tests/setup.ts'],
  collectCoverageFrom: [
    'src/**/*.ts{,x}',
    '!**/node_modules/**',
  ],
};
