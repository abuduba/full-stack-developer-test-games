import * as React from 'react';
import { SlotMachine } from './app/slot-machine';

export class App extends React.Component {
  public render() {
    return (
      <>
      <h2>The Slot Machine demo</h2>
      <SlotMachine/>
      </>
    );
  }
}
