import * as React from 'react';
import { IReel } from '../model';
import { SlotMachineReel } from './slot-machine-reel';

interface IGameMachineProps {
  reels: IReel[];
}

export class SlotMachinePanel extends React.Component<IGameMachineProps> {
  public render() {
    const { reels } = this.props;
    return (
      <div className="panel">
        {
          reels.map((reel: IReel, index: number) => (
            <SlotMachineReel slots={reel.slots} key={index}/>
          ))
        }
      </div>
    );
  }
}
