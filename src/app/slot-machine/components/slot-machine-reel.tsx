import * as React from 'react';

import { SlotMachineSlot } from './slot-machine-slot';

export interface ISlot {
  symbol: string;
  active: boolean;
}

interface IReelProps {
  slots: ISlot[];
}

export class SlotMachineReel extends React.PureComponent<IReelProps> {
  public render() {
    const slots = this.props.slots.map((slot: ISlot, index: number) => (
      <SlotMachineSlot {...slot} key={index} />
    ));
    return (
      <div className="reel">
        {slots}
      </div>
    );
  }
}
