import * as React from 'react';

interface ISlotMachineResult {
  winnings: number[];
}

export class SlotMachineResult extends React.Component<ISlotMachineResult> {
  public render(): JSX.Element {
    const { winnings } = this.props;
    const sum = winnings.reduce((a, b) => a + b, 0);
    const text = winnings.length > 1 ? ` (${winnings.join('+')})` : null;
    return (
      <div className="result">
        Winnings: {sum}{text}
      </div>
    );
  }
}
