import * as React from 'react';
import { ISlot } from './slot-machine-reel';
import classNames from 'classnames';

export class SlotMachineSlot extends React.PureComponent<ISlot> {
  public render(): JSX.Element {
    const { active, symbol } = this.props;
    return (
      <div className={classNames('slot', { active })}>
        {symbol}
      </div>
    );
  }
}
