import * as React from 'react';
import { getNormalizedState } from '../selectors';
import { ApiTrigger } from '../../../shared/api/api-trigger';
import { SlotMachinePanel } from './slot-machine-panel';
import { SlotMachineResult } from './slot-machine-result';
import { ISlotMachineState, ISlotMachineStateResponse } from '../../../model';

interface IAppState {
  gameState: ISlotMachineState | null;
  winningIndex: number;
  isSpinDisabled: boolean;
}

export class SlotMachine extends React.Component<{}, IAppState> {
  private timeout: number = -1;
  constructor(props: {}) {
    super(props);
    this.state = {
      gameState: null,
      winningIndex: 0,
      isSpinDisabled: false,
    };
  }

  public render() {
    const { gameState, isSpinDisabled } = this.state;
    const loaded = Boolean(gameState);

    return (
      <div className="slot-machine">
        <div className="panel-container">
          {
            loaded ? this.renderSlotMachine() : 'Press Spin button to start'
          }
        </div>
        <ApiTrigger<ISlotMachineStateResponse>
          resource="/spin"
          method="post"
          onSuccess={(data) => this.storeData(data.gameState)}
          render={
            (fetchData) => (
              <button disabled={isSpinDisabled} className="spin-button" onClick={fetchData}>
                {isSpinDisabled ? 'Showing winnings...' : 'Spin'}
              </button>
            )
          }
        />
      </div>
    );
  }

  private renderSlotMachine() {
    const { gameState, winningIndex } = this.state;
    const reels = getNormalizedState(gameState!, winningIndex);
    const winnings = this.state.gameState!.winnings.slice(0, winningIndex + 1).map((w) => w.winnings);

    return (
      <>
        <SlotMachinePanel reels={reels}/>
        <SlotMachineResult winnings={winnings}/>
      </>
    );
  }

  private storeData(gameState: ISlotMachineState) {
    clearTimeout(this.timeout);
    const multipleWinnings = gameState!.winnings.length > 1;
    this.setState(
      { gameState, winningIndex: 0, isSpinDisabled: multipleWinnings },
      () => {
        if (multipleWinnings) {
          this.nextWinningsIndex();
        }
      },
    );
  }

  private nextWinningsIndex() {
    this.timeout = window.setTimeout(
      () => {
        this.setState(
          (prevState: IAppState) => ({
            winningIndex: (prevState.winningIndex + 1) % prevState.gameState!.winnings.length,
          }),
          () => {
            if (this.state.winningIndex < this.state.gameState!.winnings.length - 1) {
              this.nextWinningsIndex();
            } else {
              this.setState({ isSpinDisabled: false });
            }
          },
        );
      },
      1000,
    );
  }
}
