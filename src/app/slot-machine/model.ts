export interface ISlot {
  symbol: string;
  active: boolean;
}

export interface IReel {
  slots: ISlot[];
}
