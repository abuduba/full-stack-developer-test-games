
import { IReel } from './model';
import { ISlotMachineState } from '../../model';

export const getNormalizedState = (state: ISlotMachineState, winningIndex: number): IReel[] => {
  const { panel, winnings } = state;
  const reels = panel.reels.map((reelItem, reelIndex) => {
    const slots = reelItem.symbols.map((symbol, symbolIndex) => {
      const active = winnings.length > 0 && Boolean(winnings[winningIndex].coords
        .find(({ reel, row }) => reelIndex === reel && symbolIndex === row));
      return { symbol, active };
    });
    return { slots };
  });

  return reels;
};
