import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './app';
import { ApiContext } from './shared/context/api-context';
import './styles.sass';

ReactDOM.render(
  <ApiContext.Provider value={{ host: 'localhost', baseUrl: '/api', fetch: (...args) => fetch(...args) }}>
    <App/>
  </ApiContext.Provider>,
  document.getElementById('root'),
);
