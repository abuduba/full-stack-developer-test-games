export interface ICoord {
  reel: number;
  row: number;
}

export interface IReel {
  symbols: string[];
}

export interface IPanel {
  reels: IReel[];
}

export interface IWinning {
  winnings: number;
  symbols: string[];
  coords: ICoord[];
}

export interface ISlotMachineState {
  panel: IPanel;
  winnings: IWinning[];
}

export interface ISlotMachineStateResponse {
  gameState: ISlotMachineState;
}
