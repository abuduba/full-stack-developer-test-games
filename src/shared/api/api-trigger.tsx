import * as React from 'react';
import { ApiContext } from '../context/api-context';

interface IApiTriggerProps<T> {
  resource: string;
  method: string;
  onSuccess: (data: T) => void;
  onError?: (err: Error) => void;
  render: (fetchData: () => void) => React.ReactNode;
}

export class ApiTrigger<T> extends React.Component<IApiTriggerProps<T>> {
  public static contextType = ApiContext;

  public componentDidMount() {
    if (!this.context) {
      throw new Error('There is no ApiContextProvider');
    }
  }

  public render() {
    return this.props.render(() => this.fetchData());
  }

  private async fetchData() {

    const { resource, method, onSuccess, onError } = this.props;
    try {
      const response: T = await this.context.fetch(`${this.context.baseUrl}/${resource}`, { method })
        .then((r: Response) => r.json());

      onSuccess(response);
    } catch (err) {

      if (onError) {
        onError(err);
      } else {
        throw err;
      }
    }
  }
}
