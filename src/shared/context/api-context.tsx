import { createContext } from 'react';
interface IApiContext {
  host: string;
  baseUrl: string;
  fetch: (input: RequestInfo, init: RequestInit) => Promise<Response>;
}

export const ApiContext = createContext<IApiContext>(null!);
