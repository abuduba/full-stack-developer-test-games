import * as React from 'react';
import { shallow } from 'enzyme';
import { SlotMachinePanel } from '../../src/app/slot-machine/components/slot-machine-panel';

describe('SlotMachineSlot Component should', () => {
  it('should render properly', () => {
    const component = shallow(
      <SlotMachinePanel reels={[
        {
          slots: [
            { active: true, symbol: 'AA' },
            { active: false, symbol: 'BB' },
          ]
        }]}
      />
    );

    expect(component).toMatchSnapshot();
  });

});
