import * as React from 'react';
import { shallow } from 'enzyme';
import { SlotMachineReel } from '../../src/app/slot-machine/components/slot-machine-reel';

describe('SlotMachineReel Component should', () => {
  it('should render properly', () => {
    const component = shallow(
      <SlotMachineReel
        slots={[{ symbol: 'AA', active: true }, { symbol: 'BB', active: false }]}
      />);

    expect(component).toMatchSnapshot();
  });
});
