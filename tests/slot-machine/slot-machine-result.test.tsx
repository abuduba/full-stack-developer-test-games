import * as React from 'react';
import { shallow } from 'enzyme';
import { SlotMachineResult } from '../../src/app/slot-machine/components/slot-machine-result';

describe('SlotMachineResult component should', () => {
  it('should render properly', () => {
    const component = shallow(
      <SlotMachineResult winnings={[]}/>
    );

    expect(component).toMatchSnapshot();
  });

  it.each([
    [
      '0',
      []
    ],
    [
      '0',
      [0]
    ],
    [
      '20',
      [20],
    ],
    [
      '70 (20+50)',
      [20, 50],
    ]
  ])('should render "Winnings %s" when %s is provided', (expected: string, wininngs: number[]) => {
    const component = shallow(
      <SlotMachineResult winnings={wininngs}/>
    );

    expect(component.text()).toBe(`Winnings: ${expected}`);
  });
});
