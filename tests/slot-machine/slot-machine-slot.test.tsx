import * as React from 'react';
import { shallow } from 'enzyme';
import { SlotMachineSlot } from '../../src/app/slot-machine/components/slot-machine-slot';

describe('SlotMachineSlot Component should', () => {
  it('should render properly', () => {
    const component = shallow(
      <SlotMachineSlot symbol="AA" active={false}/>
    );

    expect(component).toMatchSnapshot();
  });

  it('should not have active class by default', () => {
    const component = shallow(
      <SlotMachineSlot symbol="AA" active={false}/>
    );

    expect(component.hasClass('active')).toBe(false);
  });


  it('should add an active class when the active prop is passed', () => {
    const component = shallow(
      <SlotMachineSlot symbol="AA" active/>
    );

    expect(component.hasClass('active')).toBe(true);
  });
});
