import * as React from 'react';
import { shallow, mount, ReactWrapper } from 'enzyme';
import { SlotMachine } from '../../src/app/slot-machine/components/slot-machine';
import { ApiContext } from '../../src/shared/context/api-context';

import * as mock from './gameStateMock.json';
import { SlotMachinePanel } from '../../src/app/slot-machine/components/slot-machine-panel';

jest.useFakeTimers();

describe('SlotMachine Component', () => {
  it('should render properly', () => {
    const component = shallow(
      <SlotMachine/>
    );

    expect(component).toMatchSnapshot();
  });

  describe('when spin button is clicked', () => {
    let component: ReactWrapper<SlotMachinePanel>;
    beforeEach((done) => {
      jest.useFakeTimers()
      const promise = Promise.resolve(mock);
      component = mount(
        <ApiContext.Provider value={{ host: 'lala', fetch: jest.fn().mockResolvedValue({ json: () => promise }), baseUrl: '/' }} >
          <SlotMachine/>
        </ApiContext.Provider>
      );
      component.find('.spin-button').simulate('click');

      // @TODO: KILL ME! Not sure how to proceed with the async setState calls. Yet.
      process.nextTick(() => {
        component.update();
        done();
      })

    });

    it('should render panel', () => {
      expect(component.find(SlotMachinePanel).exists()).toBe(true);
    });

    it('should render panel with gameState', () => {
      expect(component.find(SlotMachinePanel).prop('reels')).toMatchSnapshot()
    });

    it('should change text on the spin button', () => {
      expect(component.find('.spin-button').text()).toBe('Showing winnings...');
    });

    it('should proceed with the result', () => {
      jest.advanceTimersByTime(10000);
      expect(component.find('.spin-button').text()).toBe('Spin');
    });
  });
});
