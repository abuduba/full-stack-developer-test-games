const path = require('path');

'use strict';

module.exports = {
  mode: 'development',
  devtool: 'eval-cheap-module-source-map',
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  entry: './src/index.tsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist/'
  },
  module: {
    rules: [
      {
        test: /.tsx?$/,
        loader: 'ts-loader',
        options: {
          configFile: 'tsconfig.webpack.json'
        }
      },
      {
        test: /\.sass$/,
        loader: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  devServer: {
    contentBase: './public',
    port: process.env.WDS_PORT,
    proxy: {
      '/api': {
        target: `http://localhost:${process.env.API_PORT}`,
        secure: true,
        changeOrigin: true,
      }
    }
  }
};
